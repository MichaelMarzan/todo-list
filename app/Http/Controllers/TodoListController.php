<?php

namespace App\Http\Controllers;
use App\Models\ListItem;

use Illuminate\Http\Request;

class TodoListController extends Controller
{
    //
public function index(){
    return view('welcome', ['listItems' => ListItem::get()]);
}


    public function saveItem(Request $request){
        $newListItem = new ListItem;
        $newListItem->name = $request->listItem;
        $newListItem->is_complete=0;
        $newListItem->save();
        return redirect('/');
    }

public function complete($id){
  $listItem = ListItem::find($id);
  $listItem-> is_complete = 1;
  $listItem->save();

    return redirect('/');
} 

public function incomplete($id){
    $listItem = ListItem::find($id);
    $listItem-> is_complete = 0;
    $listItem->save();
  
      return redirect('/');
  } 
public function delete($id){
    $listItem = ListItem::find($id);
    $listItem->delete();
    return redirect('/');

}

}
